# Tutoriel : passerelle e-mail sortante

Auteur : David Mercereau (retzo.net)

Contexte : monter une passerelle e-mail sortante

## Pré-requis

* Debian 11 installé
* Accès root

Les petits [outils](https://david.mercereau.info/post-install-outils-indispensables/#Outils_pour_les_serveurs) "post install" que j'aime bien avoir : 

```bash
apt install aptitude
aptitude install htop iftop iotop screen nmap tcpdump lsof iptraf dnsutils mc ncdu whois rsync tree
```

## Sécurité



## Postfix / SMTP

```
aptitude install postfix 
```

#### SSL/TLS

```
aptitude install certbot
```



## Anti-spam

## Outils

#### pflogsumm

Permet de générer des e-mails de rapport de log de ce type

```
Postfix log summaries for Nov  1

Grand Totals
------------
messages

    123   received
   1646   delivered
      0   forwarded
      1   deferred  (10  deferrals)
      0   bounced
     34   rejected (2%)
      0   reject warnings
      0   held
      0   discarded (0%)

  26257k  bytes received
 518737k  bytes delivered
     26   senders
      7   sending hosts/domains
    374   recipients
     47   recipient hosts/domains


message deferral detail
-----------------------
  smtp (total: 10)
        10   Host or domain name not found. Name service error for name=retzien.com type=MX: Host not found, try again

message reject detail
---------------------
  RCPT
    454 4.7.1 <****.scanme.org!relaytest@[149.91.81.117]>: Relay access denied; from=<********@[149.91.81.117]> to=<****.scanme.org!relaytest@[149.91.81.117]> proto=ESMTP helo=<*****scanme.org> (total: 2)
       [...]

Per-Hour Traffic Summary
------------------------
    time          received  delivered   deferred    bounced     rejected
    --------------------------------------------------------------------
    0000-0100           0          0          1          0          0 
    0100-0200           1          1          1          0          0 
    0200-0300           0          0          1          0          0 
    0300-0400           0          0          1          0          0 
    0400-0500           1          1          1          0          0 
    0500-0600           1          1          1          0          0 
    0600-0700           0          0          0          0          0 
    0700-0800           0          0          1          0          0 
    0800-0900           4          2          1          0          0 
    0900-1000           1          0          1          0         32 
    1000-1100          10        193          1          0          1 
    1100-1200           6         34          0          0          0 
    1200-1300          11        210          0          0          0 
    1300-1400           1          1          0          0          0 
    1400-1500          11        205          0          0          0 
    1500-1600           5          8          0          0          0 
    1600-1700          14        244          0          0          0 
    1700-1800           1          1          0          0          0 
    1800-1900          22        255          0          0          1 
    1900-2000          16        246          0          0          0 
    2000-2100           6         26          0          0          0 
    2100-2200           0          0          0          0          0 
    2200-2300           8        191          0          0          0 
    2300-2400           4         27          0          0          0 

Host/Domain Summary: Message Delivery 
--------------------------------------
 sent cnt  bytes   defers   avg dly max dly host/domain
 -------- -------  -------  ------- ------- -----------
    838   277526k       0     2.6 s   12.0 s  gmail.com
    178    49907k       0     1.3 s    8.3 s  hotmail.fr
    147    46930k       0     1.4 s    8.3 s  free.fr
     99    32175k       0     1.5 s    9.0 s  hotmail.com
     75    22398k       0     1.4 s    9.2 s  sfr.fr
     67    19919k       0     1.4 s    7.8 s  laposte.net
     21     7391k       0     3.5 s   11.0 s  mailoo.org
     14     4927k       0    19.5 s    1.3 m  mac.com[
     [...]

Host/Domain Summary: Messages Received 
---------------------------------------
 msg cnt   bytes   host/domain
 -------- -------  -----------
     99    25837k  retzien.fr
     98    20807k  retzo.net
     14   330007   ****.retzien.net
      4    49883   bocalenbalade.com
      3     6648   ****
      1    32105   mailgw1.retzo.net
      1     6475   tous-en-mer.org

Senders by message count
------------------------
     [...]
```

Installation : 

```bash
apt install pflogsumm
```

Mise en tâche cron, après le logrotate, sur le log de la veille : 

```bash
10 8 * * * /usr/sbin/pflogsumm -d yesterday --problems_first  --rej_add_from   --verbose_msg_detail -q /var/log/mail.log.1 | mail -s  "pflogsumm `hostname`" `hostname`@retzo.net
```

#### check_rbl

* https://github.com/matteocorti/check_rbl

Ce petit script perl permet de vérifier votre présence dans les différents RBL (conçu pour nagios il peut être utilisé sans...)

```bash
cd /opt
git clone https://github.com/matteocorti/check_rbl.git
# S'il vous manque des dépendances : 
cpan install Capture::Tiny # Exemple pour le module Capture::Tiny manquant
# tester : 
/usr/bin/perl /opt/bin/check_rbl/check_rbl -H  $(hostname) --extra-opts=rbl@/opt/bin/check_rbl/check_rbl.ini 
```

Vérification quotidienne en tâche cron : 

```bash
# Avoir paramétré les variables dans la crontab : 
# MAILFROM="mailgw2@retzo.net"
# MAILTO="root@retzo.net"
16 6 * * * /usr/bin/perl /opt/bin/check_rbl/check_rbl -H  $(hostname) --extra-opts=rbl@/opt/bin/check_rbl/check_rbl.ini -c 1 -w 1 | grep -v "plugin timed out" 
```
