# Point des différents cas de figures dans les logs Postfix

Plusieurs prrocess/queue, Dans l'odre des queue : 

* SMTPD 
* CLEANUP
* QMGR
* SMTP/ERROR

## smtpd

Connexion du serveur entrant

### Contenu 

* smtpd_client : contenu hostname du serveur, derrière "client="
* smtpd_usernames  : le nom d'utilisateur SI authentification SMTP il y a, derrière "sasl__username="

```
Jun 22 21:03:30 mailgw2 postfix/smtpd[1081847]: 0B5EE1F64F: client=master.retzo.net[159.69.124.127]
```

```
Feb 23 17:29:28 vps-33b69a8e postfix/smtpd[15662]: 2488021C9E: client=mailgw2.retzo.net[116.203.155.119], sasl_method=LOGIN, sasl_username=_____@retzo.net
```

Cas "NOQUEUE" rejeté par une politique 

* smtpd_recipient_restrictions = check_recipient_access 

```
Jun 22 20:22:20 mailgw2 postfix/smtpd[1078793]: NOQUEUE: reject: RCPT from master.retzo.net[159.69.124.127]: 550 5.7.1 <________@retzien.fr>: Recipient address rejected: Blacklisted; from=<______@retzo.net> to=<________@retzien.fr> proto=ESMTP helo=<master.retzo.net>
```

* smtpd_sender_restrictions = check_sender_access 

```
Jun 22 20:18:22 mailgw2 postfix/smtpd[1078391]: NOQUEUE: reject: RCPT from master.retzo.net[159.69.124.127]: 550 5.7.1 <___________@mercereau.info>: Sender address rejected: Blacklisted; from=<_________@mercereau.info> to=<_________@retzien.fr> proto=ESMTP helo=<master.retzo.net>
```

### Regex 

* Sans authentification : 

```
/(?<queueId>[A-Z0-9]{10}): client=(?<smtpd_client>[a-z0-9\-.\[\]]+)$
```

* Avec authentification : 

```
/(?<queueId>[A-Z0-9]{10}): client=(?<smtpd_client>[a-z0-9\-.\[\]]+), sasl_method=LOGIN, sasl_username=(?<smtpd_auth>\S+)$/
```

* NOQUEUE 

```
/(?<queueId>NOQUEUE): reject: RCPT from (?<smtpd_client>.+): (?<smtpd_codeError>[0-9\s\.]+) (?<msg>.+); from=<(?<from>\S+)> to=<(?<to>\S+)> proto=ESMTP helo=<(?<helo>\S+)>$/
```

## cleanup

Filtre, antispam, traitement...

### Contenu

* msgsmtpd_client
* from
* to
* msg

Exemple de Grub sur rspamd 

```
Feb 23 17:29:28 vps-33b69a8e postfix/cleanup[17950]: 2488021C9E: milter-reject: END-OF-MESSAGE from mailgw2.retzo.net[116.203.155.119]: 5.7.1 Gtube pattern; from=<root@mailgw2.retzo.net> to=<david@retzo.net> proto=ESMTP helo=<mailgw2.retzo.net>
```

* message-id

```
Mar 21 21:40:58 mailgw1 postfix/cleanup[3603941]: 41CBEA4B68: message-id=<91f5b140-170d-f251-4d24-6f7997b12f0b@******.fr>
Mar 21 21:44:30 mailgw1 postfix/cleanup[3604142]: 62AE3A4B68: message-id=<vB1ylXRt5kQ7ovP01YZdrfPB0HovZ04OwxSr3deB68@*******.fr>
```

### Regex

* Grub sur rspamd 

```
/(?<queueId>[A-Z0-9]{10}): (?<cleanup>.+) from (?<smtpd_client>[a-z0-9\-.\[\]]+): (?<msg>.+); from=<(?<from>.+)> to=<(?<to>.+)> proto=ESMTP helo=<(?<smtp_helo>.+)>
```

* Message ID

```
/(?<queueId>[A-Z0-9]{10}): message-id=<(?<messageid>\S+)>/
```

## qmgr

Queue loca

### Contenu

* from : du mail
* size : taille 
* nrcpt
* qmgr_msg

Cas normal : 

```
Jun 23 09:12:30 mailgw1 postfix/qmgr[866265]: 36C38A2515: from=<david@mercereau.info>, size=2660, nrcpt=1 (queue active)
```

Cas de retour du message (suite à un bounce côté processus smtp)

```
Feb 19 19:11:54 mailgw1 postfix/qmgr[1701734]: 43230A47D8: from=<____________@retzien.fr>, status=expired, returned to sender
```

### Regex

```
/(?<queueId>[A-Z0-9]{10}): from=<(?<from>\S+)>, size=(?<qmgr_size>\S+), nrcpt=(?<qmgr_nrcpt>\S+) \((?<qmgr_msg>.+)\)/
/(?<queueId>[A-Z0-9]{10}): from=<(?<from>\S+)>, status=(?<qmgr_status>\S+), (?<qmgr_msg>.+)$
```

## smtp 

Distribution du courriel

### Contenu

* to : du mail
* smtp_relay : serveur destinataire  
* smtp_status
* msg

#### status=sent

```
Feb 20 23:18:30 mailgw1 postfix/smtp[2087759]: C3FEDA4857: to=<_______@em9713.email.wetransfer.com>, relay=mx.sendgrid.net[167.89.123.50]:25, delay=0.91, delays=0.04/0/0.68/0.19, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as 85E0A501053)
```

```
Feb 20 22:12:31 mailgw1 postfix/smtp[2080056]: 87D68A4854: to=<_______>, relay=gmail-smtp-in.l.google.com[108.177.15.27]:25, delay=0.73, delays=0.1/0.04/0.1/0.49, dsn=2.0.0, status=sent (250 2.0.0 OK  1676927551 g14-20020adff3ce000000b002c54ffb5972si4166062wrp.77 - gsmtp)
```

```
Feb 20 22:12:31 mailgw1 postfix/smtp[2080057]: 87D68A4854: to=<__________@hotmail.com>, relay=hotmail-com.olc.protection.outlook.com[104.47.18.225]:25, delay=1.2, delays=0.1/0.06/0.23/0.79, dsn=2.6.0, status=sent (250 2.6.0 <327602709.6481227.1676927541341@mail.yahoo.com> [InternalId=17098264808970, Hostname=PAXP192MB1461.EURP192.PROD.OUTLOOK.COM] 69098 bytes in 0.170, 395.335 KB/sec Queued mail for delivery -> 250 2.1.5)
```

#### status=deferred

Rejet d'un nom de domaine incorrect : 

```
Feb 20 22:32:34 mailgw1 postfix/smtp[2082531]: 3A8C4A4835: to=<_________@gmail.fr>, relay=none, delay=25764, delays=25734/0.05/30/0, dsn=4.4.1, status=deferred (connect to gmail.fr[142.250.179.69]:25: Connection timed out)
```

Rejet d'un mail considéré comme SPAM par google : 

```
Feb 20 23:17:06 mailgw1 postfix/smtp[2087759]: EC8CDA4854: to=<___________@gmail.com>, relay=alt1.gmail-smtp-in.l.google.com[142.250.153.27]:25, delay=581, delays=581/0.01/0.31/0.15, dsn=4.7.0, status=deferred (host alt1.gmail-smtp-in.l.google.com[142.250.153.27] said: 421-4.7.0 [149.91.81.117      15] Our system has detected that this message is 421-4.7.0 suspicious due to the very low reputation of the sending domain. To 421-4.7.0 best protect our users from spam, the message has been blocked. 421-4.7.0 Please visit 421 4.7.0  https://support.google.com/mail/answer/188131 for more information. m7-20020aa7d347000000b004aac89c74a9si2015815edr.630 - gsmtp (in reply to end of DATA command))
```

Utilisateur inconnu

```
Feb 19 16:51:59 mailgw1 postfix-slow/smtp[1740629]: 43230A47D8: to=<___________@laposte.net>, relay=smtpz4.laposte.net[160.92.124.66]:25, delay=424658, delays=424651/0/6.9/0.24, dsn=4.2.1, status=deferred (host smtpz4.laposte.net[160.92.124.66] said: 450 4.2.1 <brimir9@laposte.net>: Recipient address rejected: this mailbox is inactive and has been disabled (in reply to RCPT TO command))
Feb 19 16:51:59 mailgw1 postfix-slow/smtp[1740629]: 43230A47D8: to=<______@laposte.net>, relay=smtpz4.laposte.net[160.92.124.66]:25, delay=424658, delays=424651/0/6.9/0.24, dsn=4.2.1, status=deferred (host smtpz4.laposte.net[160.92.124.66] said: 450 4.2.1 <brimir9@laposte.net>: Recipient address rejected: this mailbox is inactive and has been disabled (in reply to RCPT TO command))
```

Remis en file d'attente : 

```
Jan 19 11:52:53 mailgw1 postfix/error[813011]: C0662A2360: to=<*******@*******.fr>, relay=none, delay=30, delays=0.04/30/0/0.01, dsn=4.4.1, status=deferred (delivery temporarily suspended: connect to mail.******.fr[82.65.***.***]:25: Connection timed out)
```

#### status=bounced

Rejeter pour SPAM : 

```
Feb 20 22:19:19 mailgw1 postfix/smtp[2080795]: C40D6A4856: to=<_________@framalistes.org>, relay=rod3.framasoft.org[176.9.118.242]:25, delay=0.61, delays=0.01/0/0.19/0.42, dsn=5.7.1, status=bounced (host rod3.framasoft.org[176.9.118.242] said: 554 5.7.1 Spam message rejected (in reply to end of DATA command))
```

Utilisateur inconnu 

```
Feb 20 17:15:54 mailgw1 postfix/smtp[2042001]: 9CDBAA4854: to=<________@mediatheque-diocese44.fr>, relay=mx4.mail.ovh.net[178.32.124.207]:25, delay=6.7, delays=0.04/0/6.5/0.06, dsn=5.1.1, status=bounced (host mx4.mail.ovh.net[178.32.124.207] said: 550 5.1.1 <lKxj85yvmTIBueI5JgjArGH7-xaZMhSI1uMbNcA@mediatheque-diocese44.fr>: Recipient address rejected: User unknown (in reply to RCPT TO command))
Feb 19 11:48:17 mailgw1 postfix-slow/smtp[1701891]: 6CCF0A482C: to=<_________@orange.fr>, relay=smtp-in.orange.fr[80.12.26.32]:25, delay=2.6, delays=0.14/0.14/0.08/2.2, dsn=5.1.1, status=bounced (host smtp-in.orange.fr[80.12.26.32] said: 550 5.1.1 ThEtp39sv55u2 Adresse d au moins un destinataire invalide. Invalid recipient. OFR_416 [416] (in reply to RCPT TO command))
Feb 17 11:11:23 mailgw1 postfix/smtp[1094068]: EC564A4808: to=<________@hotmail.com>, relay=hotmail-com.olc.protection.outlook.com[104.47.73.161]:25, delay=1.5, delays=0.12/0.01/1.2/0.21, dsn=5.5.0, status=bounced (host hotmail-com.olc.protection.outlook.com[104.47.73.161] said: 550 5.5.0 Requested action not taken: mailbox unavailable (S2017062302). (in reply to RCPT TO command))

```

SPF fail :

```
Feb 20 15:21:12 mailgw1 postfix/smtp[2027927]: 5D732A4835: to=<_______@estay.fr>, relay=mail-fr.securemail.pro[81.88.48.101]:25, delay=2.3, delays=1.6/0/0.25/0.42, dsn=5.2.0, status=bounced (host mail-fr.securemail.pro[81.88.48.101] said: 552 5.2.0 U72Wp2yWfYSjbU72Wpcun4 Received-SPF: fail (in reply to end of DATA command))
information. g6-20020a05600c310600b003dc4ed6e889si13477836wmo.117 - gsmtp (in reply to end of DATA command)
Feb 20 15:21:12 mailgw1 postfix/smtp[2027927]: 5D732A4835: to=<__________@estay.fr>, relay=mail-fr.securemail.pro[81.88.48.101]:25, delay=2.3, delays=1.6/0/0.25/0.42, dsn=5.2.0, status=bounced (host mail-fr.securemail.pro[81.88.48.101] said: 552 5.2.0 U72Wp2yWfYSjbU72Wpcun4 Received-SPF: fail (in reply to end of DATA command))
Feb 20 15:15:17 mailgw1 postfix/smtp[2027258]: 484E7A4835: to=<_________@gmail.com>, relay=gmail-smtp-in.l.google.com[108.177.15.27]:25, delay=2.2, delays=1.6/0.08/0.2/0.33, dsn=5.7.26, status=bounced (host gmail-smtp-in.l.google.com[108.177.15.27] said: 550-5.7.26 This message does not pass authentication checks (SPF and DKIM both 550-5.7.26 do not pass). SPF check for [orange.fr] does not pass with ip: 550-5.7.26 [149.91.81.117].To best protect our users from spam, the message has 550-5.7.26 been blocked. Please visit 550-5.7.26  https://support.google.com/mail/answer/81126#authentication for more 550 5.7.26 information. q18-20020adff792000000b002c567dec2f8si378566wrp.1047 - gsmtp (in reply to end of DATA command))
```

Over quota

```
Feb 19 19:52:18 mailgw1 postfix/smtp[1763471]: 59B73A482C: to=<____@icloud.com>, relay=mx01.mail.icloud.com[17.57.156.30]:25, delay=2.8, delays=0.04/0/0.65/2.1, dsn=5.2.2, status=bounced (host mx01.mail.icloud.com[17.57.156.30] said: 552 5.2.2 <_______@icloud.com>: user is over quota (in reply to RCPT TO command))
```



### Regex

```
/^(?<queueId>[A-Z0-9]{10}): to=<(?<to>\S+)>, relay=(?<smtp_relay>\S+), delay=(?<smtp_delay>\S+), delays=(?<smtp_delays>\S+), dsn=(?<smtp_dsn>\S+), status=(?<smtp_status>\S+) \((?<msg>.+)\)/
```

## Discussion complète

Pour un retour "bounce"

```
Feb 19 19:11:53 mailgw1 postfix/qmgr[1701734]: 43230A47D8: from=<__________@retzien.fr>, size=7433, nrcpt=38 (queue active)
Feb 19 19:11:53 mailgw1 postfix-slow/smtp[1758396]: 43230A47D8: to=<__________@laposte.net>, relay=smtpz4.laposte.net[160.92.
124.66]:25, delay=433052, delays=433052/0.06/0.18/0.07, dsn=4.2.1, status=deferred (host smtpz4.laposte.net[160.92.124.66]
 said: 450 4.2.1 <__________@laposte.net>: Recipient address rejected: this mailbox is inactive and has been disabled (in rep
ly to RCPT TO command))
Feb 19 19:11:54 mailgw1 postfix/qmgr[1701734]: 43230A47D8: from=<__________@retzien.fr>, status=expired, returned t
o sender
Feb 19 19:11:54 mailgw1 postfix/cleanup[1758398]: A8614A482C: message-id=<20230219181154.A8614A482C@mailgw1.retzo.net>
Feb 19 19:11:54 mailgw1 postfix/bounce[1758397]: 43230A47D8: sender non-delivery notification: A8614A482C
Feb 19 19:11:54 mailgw1 postfix/qmgr[1701734]: A8614A482C: from=<>, size=9685, nrcpt=1 (queue active)
Feb 19 19:11:54 mailgw1 postfix/qmgr[1701734]: 43230A47D8: removed
```

Pour un e-mail avec plusieurs destinataire : 

```
Feb 14 18:54:21 mailgw1 postfix/smtpd[256498]: 43230A47D8: client=82-64-11-134.subs.proxad.net[82.64.11.134]
Feb 14 18:54:21 mailgw1 postfix/cleanup[256501]: 43230A47D8: message-id=<cdc4a64e988a3cd0d16f46e5d3351c8a@retzien.fr>
Feb 14 18:54:21 mailgw1 postfix/qmgr[1701734]: 43230A47D8: from=<__________@retzien.fr>, size=7433, nrcpt=38 (queue
 active)
Feb 14 18:54:21 mailgw1 postfix-slow/smtp[256502]: 43230A47D8: to=<__________@free.fr>, relay=mx1.free.fr[212.27.48.6]:25, d
elay=0.45, delays=0.08/0.05/0.19/0.13, dsn=5.2.1, status=bounced (host mx1.free.fr[212.27.48.6] said: 550 5.2.1 This mailb
ox has been blocked due to inactivity (UserSearch) (in reply to RCPT TO command))
Feb 14 18:54:21 mailgw1 postfix-slow/smtp[256502]: 43230A47D8: to=<__________@free.fr>, relay=mx1.free.fr[212.27.48.6]
:25, delay=0.53, delays=0.08/0.05/0.19/0.21, dsn=2.0.0, status=sent (250 OK)
Feb 14 18:54:21 mailgw1 postfix-slow/smtp[256502]: 43230A47D8: to=<__________@free.fr>, relay=mx1.free.fr[212.27.48.6]
Feb 14 18:54:22 mailgw1 postfix/smtp[256508]: 43230A47D8: to=<________@gmail.com>, relay=gmail-smtp-in.l.google.com[
108.177.15.26]:25, delay=0.9, delays=0.08/0.13/0.1/0.59, dsn=2.0.0, status=sent (250 2.0.0 OK  1676397262 g19-20020a05600c311300b003dc5c96f655si16639489wmo.132 - gsmtp) 
Feb 14 18:54:22 mailgw1 postfix/smtp[256508]: 43230A47D8: to=<________@gmail.com>, relay=gmail-smtp-in.l.google.com[108.177.15.26]:25, delay=0.9, delays=0.08/0.13/0.1/0.59, dsn=2.0.0, status=sent (250 2.0.0 OK  1676397262 g19-0020a05600c311300b003dc5c96f655si16639489wmo.132 - gsmtp)
```

Un e-mail en greylist (plusieurs) : 

```
Feb 14 16:49:37 mailgw1 postfix/smtpd[240797]: 27694A47D8: client=82-64-11-134.subs.proxad.net[82.64.11.134]
Feb 14 16:49:37 mailgw1 postfix/cleanup[240800]: 27694A47D8: message-id=<dd7ee212-07bb-69fd-b9c3-6e685742809b@retzien.fr>
Feb 14 16:49:37 mailgw1 postfix/qmgr[1701734]: 27694A47D8: from=<__________@retzien.fr>, size=199710, nrcpt=2 (queue active)
Feb 14 16:49:38 mailgw1 postfix/smtp[240801]: 27694A47D8: host gange.over-link.net[91.209.154.4] said: 450 4.2.0 <__________@__________.fr>: Recipient address rejected: Greylisting please retry in few minutes (in reply to RCPT TO command)
Feb 14 16:49:38 mailgw1 postfix/smtp[240801]: 27694A47D8: to=<__________@__________.fr>, relay=volga.over-link.net[46.183.49.21]:25, delay=1.4, delays=0.19/0.03/1/0.18, dsn=4.2.0, status=deferred (host volga.over-link.net[46.183.49.21] said: 450 4.2.0 <__________@__________.fr>: Recipient address rejected: Greylisting please retry in few minutes (it's normal) (in reply to RCPT TO command))
Feb 14 16:56:17 mailgw1 postfix/qmgr[1701734]: 27694A47D8: from=<__________@retzien.fr>, size=199710, nrcpt=2 (queue active)
Feb 14 16:56:17 mailgw1 postfix/smtp[241896]: 27694A47D8: to=<__________@__________.fr>, relay=gange.over-link.net[91.209.154.4]:25, delay=400, delays=400/0.06/0.22/0.17, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as 3F3B9F803A)
Feb 14 16:56:17 mailgw1 postfix/qmgr[1701734]: 27694A47D8: removed
```

Spam détecté/rejeté : 

```
Feb 23 17:29:28 vps-33b69a8e postfix/smtpd[15662]: 2488021C9E: client=mailgw2.retzo.net[116.203.155.119], sasl_method=LOGIN, sasl_username=__________@retzo.net
Feb 23 17:29:28 vps-33b69a8e postfix/cleanup[17950]: 2488021C9E: message-id=<GTUBE1.1010101@example.net>
Feb 23 17:29:28 vps-33b69a8e postfix/cleanup[17950]: 2488021C9E: milter-reject: END-OF-MESSAGE from mailgw2.retzo.net[116.203.155.119]: 5.7.1 Gtube pattern; from=<root@mailgw2.retzo.net> to=<david@retzo.net> proto=ESMTP helo=<mailgw2.retzo.net>
Feb 23 17:29:28 vps-33b69a8e postfix/smtpd[15662]: disconnect from mailgw2.retzo.net[116.203.155.119] ehlo=2 starttls=1 auth=1 mail=1 rcpt=1 data=0/1 quit=1 commands=7/8
```

## Stratégie d'enregistrement

Simple : 1 table, pas de clé uniq, sinon trop le bazar...

Vérification du checksum sur le client  ça évite au client de faire trop de requête vers le serveur 

* checksum crc32 de toutes les données prêtes à êtres envoyé ([crc32](https://www.php.net/manual/fr/function.crc32.php)(json_encode($data))) on le met en BD pour être sûr de pas renvoyer 2 fois la même... si le status du message change (resté en file d'attente pour relance) alors le message est renvoyé (l'heure ou le message à changé...)

Côté serveur, on fait un select avec les données qui arrivent pour voir s'il existe déjà...

En process SMTP on fait systématiquement un updates du message et du status si le reste (date, queuid, from, to est identique)

pour les dates, 2 dates sont enregistré : 

* smtpd_date : date d'arrivé sur le serveur
* smtp_date : date de sortie ou de fin de traitement 

--------

-2 table sur le serveur : 

* logs
  * QUEUE ID uniq
  * NOQUEUE = Chechsum data (envoyé donc...) < 10 caractères...
* logs_smtp
  * QUEUE ID non uniq  
  * checksum uniq constitué des données transmises
    * [crc32](https://www.php.net/manual/fr/function.crc32.php)(json_encode($data))

NON Fonctionnelle, plusieurs envoi à des moments différent, donc pas sûr que le premier bout est là alors que j'envoie la suite...

---------------

/ TOUT REPRENDRE par processus...

​            // Faire le point de quand il y a QueueID ou non (généré ?)

​            // Si c'est que le TO qui change sur certain ID, est-ce qu'on fait une sous table juste pour elle ? (simplifie)

​            // Sinon multiple QueueID mais check les autres champs...

​            // Le PID est une piste, mais ça fonctoinne pas pour le process "smtp" qui peut re-essayer plusieurs fois...

​                // bon après c'est à ce moment là qu'on fait un "check" d'autre chose + update

​            // TIMESTAMP-PID en clé primaire = bonne piste pour check les doublons avec INSERT IGNORE

​            // même pas : même timestamp, même PID...

​	

Pas possible de faire un ID unique en fonction de l'heure+serveur+PID+QueueID, c'est parfois identique

```
            Sep  2 23:45:19 mailgw1 postfix-slow/smtp[4039193]: D1552A24BB: to=<user1@yahoo.fr>, relay=mx-eu.mail.am0.yahoodns.net[188.125.72.74]:25, delay=19, delays=0.05/19/0.23/0.33, dsn=2.0.0, status=sent (250 ok dirdel 2/2)

​            Sep  2 23:45:19 mailgw1 postfix-slow/smtp[4039193]: D1552A24BB: to=<user2@yahoo.fr>, relay=mx-eu.mail.am0.yahoodns.net[188.125.72.74]:25, delay=19, delays=0.05/19/0.23/0.33, dsn=2.0.0, status=sent (250 ok dirdel 2/2)
```

