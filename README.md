# oMailgw - Gestion de passerelle(s) e-mail sortante(s)

oMailgw est un outil pour gérer un ou des passerelles (serveurs) e-mail sortant utilisant Postfix.

Le but premier est de pouvoir partager des passerelles e-mail sortante entre plusieurs acteurs du e-mail afin de mutualiser des coûts humains et matériels. Ou simplement pour offrir un service d'e-mail sortant et de donner accès à ces clients à la file d'attente, au log de trafic / erreur...

L'interface est accessible à plusieurs niveaux : 

- Pour les administrateurs des serveurs émetteurs (avant la passerelle)
- Pour les utilisateurs : ceci afin de leur permettre d’accéder à leurs logs (selon le champ from : michel@domain.fr)) ou de leurs domaines entier (@domain.fr)

oMailgw est constitué de 3 briques : 

* [omailgw-api](https://framagit.org/omailgw/omailgw-api/) : Une API en PHP(laravel)/Mysql ou est concentré le stockage des données (logs,  file d'attente, route, permissions, utilisateurs... )
* [omailgw-ui](https://framagit.org/omailgw/omailgw) : qui offre une interface web pour utiliser à l'API (static HTML/JS)
* [omailgw-cli](https://framagit.org/omailgw/omailgw-cli/) : le client qui est installé sur le(s) serveur(s)/passerelle(s) sortante(s) qui lis les logs et les transmet à l'API par HTTP. Il peut y avoir un nombre infini de "cli" par "api".

## Cas d'usage 

* Infrastructure d'hébergement e-mail avec X passerelles e-mail de sorties (pour pouvoir jongler avec les IP en cas de blacklistage)
  * oMailgw permet à l'administrateur un suivi des logs, remonté d'erreurs, statistiques d'utilisation fines
  
* Au sein d'un collectif de petit hébergeur (type [C.H.A.T.O.N.S](https://www.chatons.org/)),  pouvoir collectiviser X passerelles de sorties e-mails chez un ou plusieurs C.H.A.T.O.N.S. afin que chacun gère sa réputation e-mail mais qu'en cas de problème on puisse basculer chez les un(s) ou chez les autre(s) une partie du trafic le temps de retrouver une réputation IP viable.
  * il pourrait y avoir plusieurs oMailgw-UI (interface) pour plusieurs passerelles 
  
  ![](asset/oMailgw.png)

## Comment ça marche 

- Une interface ([oMailgw-UI](https://framagit.org/omailgw/omailgw/)) pour l'utilisateur, celle-ci communique avec l'API ([oMailgw-API](https://framagit.org/omailgw/omailgw-api/))
- Une API ([oMailgw-API](https://framagit.org/omailgw/omailgw-api/)) qui reçoit les logs de différentes passerelles ([oMailgw-cli](https://framagit.org/omailgw/omailgw-cli)) (mailgw - serveur de mail sortant) via API HTTP et en profite pour distribuer les blacklist, authentification SMTP, transport_map de postfix..
- L’utilisateur via l’UI peut voir les logs le concernant / des statistiques (notamment sur les retours d’erreur)
   - Un utilisateur peut être un chaton, qui a accès à l’ensemble des  logs qu’il émet, ou un utilisateur final qui n’a accès qu’au log des  messages qui le concerne (permission sur des champs dans les  logs)
   - Les utilisateurs qui on déjà envoyé un e-mail via les passerelles (qui apparaissent donc dans le "from" des logs) peuvent se créer un compte en toute autonomie sur la plateforme et ainsi avoir accès à leur logs d'envoi.
- Un rapport quotidien/hebo/mensuelle sur les erreurs peut être émis par e-mail ([exemple de rapport](./asset/Exemple%20-%20Rapport%20oMailgw%20Retzo.eml))

### Fonctionnalité


- Visualiser des logs de trafic e-mail sortant selon des permissions : 

   - from :  complet ou seulement le domaine
   - smtpd_username : authentification SMTP
   - server : le serveur (la passerelle) 

- Visualiser les e-mails en attentes sur les passerelles (toujours selon les permissions précédentes)
- Blacklist/hard bounces : règle de blackliste sur l'émetteur ou le destinataire par regex :

   - Exemple : Permet de bloquer en amont les destinataires qui n'existe pas afin de ne pas ternir la réputation du serveur à essayer fréquemment d'envoyer des e-mails à un destinataire qui n'existe pas.

- Gestion des authentifications SMTP (admin/root)
- Gestion des "transport_map" (root)

## Démonstration

Une interface est disponible en démonstration (v0.9) : 

* URL : https://omailgw-demo.retzo.net
* Utilisateur :

  * root@retzo.net (Rôle root)

  * admin@retzo.net (Rôle admin)

  * user@retzo.net (Rôle : user)
* Mot de passe  : password

## Capture d'écran

Toutes les captures : https://framagit.org/omailgw/omailgw-doc/-/tree/main/screenshot

Le dashboard

![Dashboard](screenshot/dashboard.png)

Affichage du tableau de trafic log :

![Trafic Log](screenshot/logtable.png)



## Auteurs

* [David Mercereau](https://david.mercereau.info/) [@kepon](https://framagit.org/kepon)
* [Pierrick Anceaux](https://github.com/Shurtugl/) [@Shurtugl](https://framagit.org/Shurtugl) 

## Licence

Licence GPL v3 http://www.gnu.org/licenses/gpl-3.0.html

Voir Licence.txt
